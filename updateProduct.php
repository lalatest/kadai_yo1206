<?php
session_start();
require_once("lib/util.php");
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre><hr>";

$gobackURL = "mainmenu.php";

// 文字エンコードの検証
// UTF-8以外の場合はエラーメッセージを出して終了
if (!cken($_POST)){
  header("Location:{$gobackURL}");
  exit();
}
//ログイン処理済かの検証
cklogin();

?>


<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>商品マスタメンテナンス  | 検索・更新</title>
  <link rel="stylesheet" href="css/styles.css">
</head>
<body>
<div class="searchupdate_form_wrapper">

<div class="search_form_wrapper">
  <p class="search_title_menu">商品コード指定</p>
  <form class="search_form" method="post" action="searchProduct.php">

        商品コード
        <input type="text" name="product_ID" size="50" value="<?= es($_SESSION['details'][0]['Product_ID'])?>"><br>
        <input type="submit" value="検索">


  </form>

</div>

<div class="update_form_wrapper">
  <form class="update_form" method="POST" action="confirmPupdate.php">
    <table class="update_form_table">
      <tr>
        <th>商品名</th>
        <td><input type="text" name="product_name" size="50"  value="<?= es($_SESSION['details'][0]['Product_Name'])?>"></td>
      </tr>
      <tr>
        <th>単価</th>
        <!-- 数字のためtell使用 -->
        <td><input type="tel" name="product_val" size="50"  value="<?= es($_SESSION['details'][0]['Product_Val'])?>"></td>
      </tr>
      <tr>
        <th>前回登録日時</th>
        <td><input type="text" name="insert_date" size="50" value="<?= es($_SESSION['details'][0]['insert_date'])?>" readonly="readonly"></td>
      </tr>
    </table>
        <div class="update_button">
          <input type="submit" value="更新">
          <button class="smallbutton"type="button" name="button_r"><a href="mainmenu.php">戻る</a></button>
        </div>
  </form>
</div>

</div>

</body>
</html>
