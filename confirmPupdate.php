<?php
session_start();
require_once("lib/util.php");
$gobackURL = "updateProduct.php";
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre><hr>";

// 文字エンコードの検証
if (!cken($_POST)){
  header("Location:{$gobackURL}");
  exit();
}
//ログイン処理済かの検証
cklogin();

//簡易入力チェック
$errors = [];
//商品名入力チェック
if(!isset($_POST['product_name']) || ($_POST['product_name'] === "")){
  $errors[] = "商品名が空欄です。";
}

//商品単価入力チェック
if(!isset($_POST['product_val']) || !ctype_digit($_POST['product_val'])){
  $errors[] = "単価には数値を入力してください。";
}

//エラーがあったとき
if(count($errors)>0){
  echo'<span class="errors">', implode('<br>', $errors), '</span>';
  echo "<hr>";
  echo '<a href="updateProduct.php">戻る</a>';
  exit();
}

// データベースユーザ
// $user = 'testuser';
// $password = 'pw4testuser';
// // 利用するデータベース
// $dbName = 'product';
// // MySQLサーバ
// $host = 'localhost';
// // MySQLのDSN文字列
// $dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";

$user = 'lala4_kadaitest';
$password = 'pw4kadaitest';
// 利用するデータベース
$dbName = 'lala4_product';
// MySQLサーバ
$host = 'mysql1.php.xdomain.ne.jp';
// MySQLのDSN文字列
$dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商品マスタメンテナンス | 更新事項確認</title>
<link href="css/styles.css" rel="stylesheet">
</head>
<body>
<div>
  <?php
  //セッションに入っている商品ＩＤの値を変数に代入
  $p_ID = $_SESSION['details'][0]['Product_ID'];
  // 商品名、単価についてはPOSTされた値を変数に代入
  $p_name = $_POST["product_name"];
  $p_val = $_POST['product_val'];

  echo "<pre>";
  print_r($p_name);
  echo "<br>";
  print_r($p_val);
  echo "</pre><hr>";

//MySQLデータベースに接続
  try {
    $pdo = new PDO($dsn, $user, $password);
    // プリペアドステートメントのエミュレーションを無効にする
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    // 例外がスローされる設定にする
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // SQL文を作る
    //情報を更新する
    $sql = "UPDATE M_product SET Product_Name= :product_name,Product_Val= :product_val,insert_date= now() WHERE Product_ID = :product_ID";
    //UPDATE `m_product` SET `Product_ID`=[value-1],`Product_Name`=[value-2],`Product_Val`=[value-3],`insert_date`=[value-4] WHERE 1
    // プリペアドステートメントを作る
    $updateProduct = $pdo->prepare($sql);
    // プレースホルダに値をバインドする
    $updateProduct->bindValue(':product_ID', $p_ID, PDO::PARAM_STR);
    $updateProduct->bindValue(':product_name', $p_name, PDO::PARAM_STR);
    $updateProduct->bindValue(':product_val', $p_val, PDO::PARAM_INT);
    //SQL文の実行
    $updateProduct->execute();

?>

    <script>alert("完了しました");
      location.href = 'search.php';
    </script>

  <?php
  exit();


  } catch (Exception $e) {
    //接続エラー
    echo '<span class="error">エラーがありました。</span><br>';
    echo $e->getMessage();
  }
  ?>
  <hr>
  <p><a href="<?php echo $gobackURL ?>">戻る</a></p>
</div>
</body>
</html>
