<?php
session_start();
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre><hr>";
require_once("lib/util.php");
// 文字エンコードの検証
if (!cken($_POST)){
  header("Location:{$gobackURL}");
  exit();
}
//ログイン処理済かの検証
cklogin();


// データベースユーザ
$user = 'testuser';
$password = 'pw4testuser';
// 利用するデータベース
$dbName = 'product';
// MySQLサーバ
$host = 'localhost';
// MySQLのDSN文字列
$dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";


?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>商品マスタメンテナンス  | 新規作成</title>
  <link rel="stylesheet" href="css/styles.css">
</head>
<body>
<div class="insert_form_wrapper">
  <form method="POST" action="confirmPinsert.php">
    <table class="insert_form_table">
      <tr>
        <th>商品コード</th>
        <td><input type="text" name="product_ID" size="50"></td>
      </tr>
      <tr>
        <th>商品名</th>
        <td><input type="text" name="product_name" size="50"></td>
      </tr>
      <tr>
        <th>単価</th>
        <!-- 数字のためtell使用 -->
        <td><input type="tel" name="product_val" size="50"></td>
      </tr>
    </table>
        <div class="insert_button">
          <input type="submit" value="新規登録">
          <button class="smallbutton"type="button" name="button_r"><a href="mainmenu.php">戻る</a></button>
        </div>
  </form>

</div>

</div>
</body>
</html>
