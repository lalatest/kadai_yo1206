<?php
session_start();
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre><hr>";
require_once("lib/util.php");

$gobackURL = "loginform.php";

// 文字エンコードの検証
if (!cken($_POST)){
  header("Location:{$gobackURL}");
  exit();
}

?>
<?php
//入力チェック
//社員IDとパスワードいずれも未入力の時
if((!isset($_POST['staff_ID']) || ($_POST['staff_ID'] === "")) && (!isset($_POST['password']) || ($_POST['password'] === ""))): ?>
<script type="text/javascript">
window.alert("社員ID・パスワードが未入力です。");
location.href = 'loginform.php';
</script>

<?php else:
  //社員IDが未入力の時
  if(!isset($_POST['staff_ID']) || ($_POST['staff_ID'] === "")){ ?>

  <script type="text/javascript">
    window.alert("社員IDが未入力です。");
    location.href = 'loginform.php';
  </script>

<?php }
  //パスワードが未入力の時
  if(!isset($_POST['password']) || ($_POST['password'] === "")){ ?>

  <script type="text/javascript">
    window.alert("パスワードが未入力です。");
    location.href = 'loginform.php';
  </script>

<?php }?>
<?php endif;?>


<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ログイン確認</title>
  <link rel="stylesheet" href="css/styles.css">
</head>
<body>
  <div>

<?php
//データベースの情報
// データベースユーザ
//localhost
// $user = 'testuser';
// $password = 'pw4testuser';
// // 利用するデータベース
// $dbName = 'staff';
// // MySQLサーバ
// $host = 'localhost';
// // MySQLのDSN文字列
// $dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";

$user = 'lala4_kadaitest';
$password = 'pw4kadaitest';
// 利用するデータベース
$dbName = 'lala4_staff';
// MySQLサーバ
$host = 'mysql1.php.xdomain.ne.jp';
// MySQLのDSN文字列
$dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";


?>
<?php
  //POSTされた値を変数に代入
  $staff_ID = $_POST["staff_ID"];
  $pw = $_POST["password"];
  //POSTされた内容の確認
  // echo "<pre>●ログイン入力内容●<br>";
  // print_r($staff_ID);
  // echo "<br>";
  // print_r($pw);
  // echo "</pre><hr>";

  //MySQLデータベースに接続する
  try {
    $pdo = new PDO($dsn, $user, $password);
    // プリペアドステートメントのエミュレーションを無効にする
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    // 例外がスローされる設定にする
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // SQL文を作る
    $sql = "SELECT Staff_ID, password FROM M_staff WHERE Staff_ID = :staff_ID";
    // プリペアドステートメントを作る
    $stm = $pdo->prepare($sql);
    // プレースホルダに値をバインドする
    $stm->bindValue(':staff_ID', $staff_ID, PDO::PARAM_STR);
    //SQL文の実行
    $stm->execute();

    // 結果の取得（連想配列で受け取る）
    $result = $stm->fetchAll(PDO::FETCH_ASSOC);
    // echo "<pre>●登録内容●<br>";
    // print_r($result[0]);
    // echo "</pre><hr>";

    //社員ＩＤがあった場合にパスワード照合
    if(count($result)>0){
      $hash = $result[0]['password'];
      //パスワードが一致する場合
      if($pw === $hash){
      // if(password_verify($pw, $hash)) {
        // echo "<h2>ログイン成功。</h2>";
        //ログインした社員ＩＤをセッションに保持
        $_SESSION['s_ID'] = $_POST["staff_ID"];
        // print_r($_SESSION);
        session_write_close();
        //メインメニューへ遷移
        // header("Location: mainmenu.php");
         echo '<script> location.replace("mainmenu.php"); </script>';
        exit();

      } else { ?>
        <!-- パスワードが一致しない場合 -->
        <script>
        window.alert("社員IDもしくはパスワードが誤っているため、ログインできません。再度入力してください。");
        location.href = 'loginform.php';
      </script>;
    <?php  }
  } else { ?>
    <!-- 社員ＩＤが存在しない場合 -->
    <script>
    window.alert("社員IDもしくはパスワードが誤っているため、ログインできません。再度入力してください。");
    location.href = 'loginform.php';
    </script>

   <?php }
  // データベース接続エラー
  } catch (Exception $e) {
    echo '<span class="error">エラーがありました。</span><br>';
    echo $e->getMessage();
  }
  ?>


</div>








  </body>
</html>
