<?php
session_start();
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre><hr>";
require_once("lib/util.php");
$gobackURL = "insert.php";

// 文字エンコードの検証
if (!cken($_POST)){
  header("Location:{$gobackURL}");
  exit();
}
//ログイン処理済かの検証
cklogin();

//入力チェック
//商品コード入力チェック
if(!isset($_POST['product_ID']) || ($_POST['product_ID'] === "")): ?>
<script type="text/javascript">
window.alert("商品コードが未入力です。");
location.href = 'insert.php';
</script>
<?php
exit();

endif;

// データベースユーザ
// $user = 'testuser';
// $password = 'pw4testuser';
// // 利用するデータベース
// $dbName = 'product';
// // MySQLサーバ
//  $host = 'localhost';
// // MySQLのDSN文字列
// $dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";

$user = 'lala4_kadaitest';
$password = 'pw4kadaitest';
// 利用するデータベース
$dbName = 'lala4_product';
// MySQLサーバ
$host = 'mysql1.php.xdomain.ne.jp';
// MySQLのDSN文字列
$dsn = "mysql:host={$host};dbname={$dbName};charset=utf8";


?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商品マスタメンテナンス  | 登録管理</title>
<style media="screen">
.insert_confirmed_wrapper{
	width: 80%;
	margin: 0 auto;
	text-align: center;
}
table {
	margin: 2em auto;
	padding: 0;
	border-collapse: collapse;
	width: 80%;
	margin: auto;
}

thead {
	background-color: #7ac2ff;
	text-align: left;
}

tr *{
	padding: : 0.5em 1em 0.5em 1em;
	width: auto;
	text-align: left;
}
tbody tr:nth-child(even) td {
	background-color: #dff0ff;
}

</style>
</head>
<body>
<div class="insert_confirmed_wrapper">
  <?php
  // POSTされた値を変数に代入
  $p_ID = $_POST["product_ID"];
  $p_name = $_POST["product_name"];
  $p_val = $_POST['product_val'];

  // // 変数の確認
  // echo "<pre>";
  // print_r($p_ID);
  // echo "<br>";
  // print_r($p_name);
  // echo "<br>";
  // print_r($p_val);
  // echo "</pre><hr>";

//MySQLデータベースに接続
  try {
    $pdo = new PDO($dsn, $user, $password);
    // プリペアドステートメントのエミュレーションを無効にする
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    // 例外がスローされる設定にする
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // SQL文を作る
    //既に存在する商品か確認
    $sql = "SELECT Product_ID FROM M_product WHERE Product_ID = :product_ID";
    // プリペアドステートメントを作る
    $stm = $pdo->prepare($sql);
    // プレースホルダに値をバインドする
    $stm->bindValue(':product_ID', $p_ID, PDO::PARAM_STR);
    //SQL文の実行
    $stm->execute();

    // 結果の取得（連想配列で受け取る）
    $result = $stm->fetchAll(PDO::FETCH_ASSOC);


    //既に存在する商品だった場合は、エラー表示し、入力フォームへ戻る
    if(count($result)>0){ ?>
        //アラート
        <script>alert("既にその商品コードは使用されています。");
        location.href = 'insert.php';

        </script>

      <?php
      exit();
      } else {
          //該当するものがない場合

          //SQL分を作る
          //新規商品を登録
          $sql = "INSERT INTO M_product VALUES (:product_ID, :product_name, :product_val, now())";
          // INSERT INTO `m_product`(`Product_ID`, `Product_Name`, `Product_Val`, `insert_date`) VALUES ('TEST02', 'TEST_ITEM2', '200', now());
          // プリペアドステートメントを作る
          $insertProduct = $pdo->prepare($sql);
          // プレースホルダに値をバインドする
          $insertProduct->bindValue(':product_ID', $p_ID, PDO::PARAM_STR);
          $insertProduct->bindValue(':product_name', $p_name, PDO::PARAM_STR);
          $insertProduct->bindValue(':product_val', $p_val, PDO::PARAM_INT);

          // SQL文を実行する
          if($insertProduct->execute()){
            $sql = "SELECT * FROM M_product";
            $insertProduct = $pdo->prepare($sql);
            $insertProduct->execute();

          // 結果の取得（連想配列で受け取る）
          $result = $insertProduct->fetchAll(PDO::FETCH_ASSOC);

          // echo "<pre>";
          // print_r($result);
          // echo "</pre><hr>";
          echo '<p>新規商品登録に成功しました。</p>';
          echo "<hr>";
          // 全商品をテーブルで表示
          echo '<p>全商品データ</p>';
            // テーブルのタイトル行
            echo "<table>";
            echo "<thead><tr>";
            echo "<th>", "商品ID", "</th>";
            echo "<th>", "商品名", "</th>";
            echo "<th>", "単価", "</th>";
            echo "<th>", "登録日時", "</th>";
            echo "</tr></thead>";
            // 値を取り出して行に表示する
            echo "<tbody>";
            foreach ($result as $row){
              // １行ずつテーブルに入れる
              echo "<tr>";
              echo "<td>", es($row['Product_ID']), "</td>";
              echo "<td>", es($row['Product_Name']), "</td>";
              echo "<td>", es($row['Product_Val']), "</td>";
              echo "<td>", es($row['insert_date']), "</td>";
              echo "</tr>";
            }
            echo "</tbody>";
            echo "</table>";
          } else {
            echo '<span class="error">追加エラーがありました。</span><br>';
          }

    }
  } catch (Exception $e) {
    //接続エラー
    echo '<span class="error">エラーがありました。</span><br>';
    echo $e->getMessage();
  }
  ?>
  <hr>
  <p><a href="<?php echo $gobackURL ?>">戻る</a></p>
</div>
</body>
</html>
